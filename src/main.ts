import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import { createAuth0, authGuard } from '@auth0/auth0-vue';
import VueFeather from 'vue-feather';
import './scss/styles.scss';

import App from './App.vue';

import Dashboard from './pages/Dashboard.vue';
import Tags from './pages/Tags.vue';
import Categories from './pages/Categories.vue';
import Callback from './pages/Callback.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', component: Dashboard },
    { path: '/categories', component: Categories },
    { path: '/tags', component: Tags },
    { path: '/callback', component: () => Callback },
  ],
});

//router.beforeEach(authGuard);

const auth0 = createAuth0({
  domain: 'dev-tk6n2ikx1x761ixd.us.auth0.com',
  clientId: 'OObQnytTuJZe2ODsv1FPK3dXvjjOHj3T',
  authorizationParams: {
    redirect_uri: `${window.location.origin}/callback`,
  },
});

const app = createApp(App);
app.use(router);
app.use(auth0);
app.component(VueFeather.name, VueFeather);
app.mount('#app');
